
Feature: My Test
  Scenario: Test my "/test" route
    Given I am on "http://localhost:8000"
    When I go to "/test"
    Then I should be on "/test"
    And I should see "Hello TestController3!"

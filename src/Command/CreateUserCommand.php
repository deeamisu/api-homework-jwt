<?php

namespace App\Command;

use App\Service\NewUserService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CreateUserCommand extends Command
{
    protected static $defaultName = 'app:create:user';

    protected $userService;

    /**
     * CreateUserCommand constructor.
     */
    public function __construct(NewUserService $userService)
    {
        parent::__construct();
        $this->userService = $userService;
    }


    protected function configure()
    {
        $this
            ->setDescription('Creates a new user')
            ->addArgument('username', InputArgument::REQUIRED, 'username')
            ->addArgument('password', InputArgument::REQUIRED, 'password')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $username = $input->getArgument('username');
        $password = $input->getArgument('password');

        $io->title('Creating user');
        $this->userService->createUser($username, $password);
        $io->success('New user created!');

        return Command::SUCCESS;
    }
}

<?php

namespace App\Controller\Rest;

use App\Entity\Training;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use JMS\Serializer\SerializerBuilder as Jms;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Class ApiTrainingController
 * @package App\Controller\Rest
 * @Rest\Route("/api")
 * @OA\Response(
 *     response=200,
 *     @Model(type=Training::class),
 *     description="Manages a training",
 *     @OA\JsonContent(
 *        type="array"
 *     )
 * )
 *
 * @Security(name="Bearer")
 */
class ApiTrainingController extends AbstractFOSRestController
{
    /**
     * Retrieves a Training resource
     * @Rest\Get("/trainings/{trainingId}")
     * @param int $trainingId
     * @return View
     */
    public function getTraining(int $trainingId): View
    {
        $training = $this->getDoctrine()->getRepository(Training::class)->find($trainingId);
        // In case our GET was a success we need to return a 200 HTTP OK response with the request object
        return View::create($training, Response::HTTP_OK);
    }

    /**
     * Retrieves all Training resources
     * @Rest\Get("/trainings")
     */
    public function getTrainings(): View
    {
        $trainings = $this->getDoctrine()->getRepository(Training::class)->findAll();
        // In case our GET was a success we need to return a 200 HTTP OK response with the request object
        return View::create($trainings, Response::HTTP_OK);
    }

    /**
     * Creates a Training resource
     * @Rest\Post("/trainings")
     * @param Request $request
     * @return View
     */
    public function postTraining(Request $request): View
    {
        $serializer = Jms::create()->build();
        $training = $serializer->deserialize($request->getContent(), Training::class, 'json');

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($training);
        try {
            $entityManager->flush();
            // In case our POST was a success we need to return a 201 HTTP CREATED response
            return View::create($training, Response::HTTP_CREATED);
        } catch (Exception $exception) {
            echo $exception->getMessage();
            // In case our POST was a disaster we need to return a 417 HTTP EXPECTATION FAILED response
            return View::create($training, Response::HTTP_EXPECTATION_FAILED);
        }
    }

    /**
     * Replaces Training resource
     * @Rest\Put("/trainings/{trainingId}")
     * @param int $trainingId
     * @param Request $request
     * @return View
     */
    public function putArticle(int $trainingId, Request $request): View
    {
        $entityManager = $this->getDoctrine()->getManager();
        $training = $entityManager->find(Training::class, $trainingId);
        if ($training) {
            $serializer = Jms::create()->build();
            /** @var Training $training */
            $training = $serializer->deserialize($request->getContent(), Training::class, 'json');
            try {
                $training->setId($trainingId);
                $entityManager->merge($training);
                $entityManager->flush();
                // In case our PUT was a success we need to return a 200 HTTP OK response with the object as a result of PUT
                return View::create($training, Response::HTTP_OK);
            } catch (Exception $exception) {
                echo $exception->getMessage();
                // In case our PUT was a disaster we need to return a 417 HTTP EXPECTATION FAILED response
                return View::create($training, Response::HTTP_EXPECTATION_FAILED);
            }
        }
        // In case our PUT hasn't found a resource, we need to return a 400 HTTP BAD REQUEST response with the wrong id as a result
        return View::create($trainingId, Response::HTTP_BAD_REQUEST);
    }

    /**
     * Removes the Training resource
     * @Rest\Delete("/trainings/{trainingId}")
     * @param int $trainingId
     * @return View
     */
    public function deleteTraining(int $trainingId): View
    {
        $entityManager = $this->getDoctrine()->getManager();
        $training = $entityManager->find(Training::class, $trainingId);
        if ($training) {
            $entityManager->remove($training);
            $entityManager->flush();
        }
        // In case our DELETE was a success we need to return a 204 HTTP NO CONTENT response. The object is deleted.
        return View::create([], Response::HTTP_NO_CONTENT);
    }
}

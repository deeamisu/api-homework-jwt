<?php

namespace App\Controller;

use App\Message\MyTestMessage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends AbstractController
{
    /**
     * @Route("/test", name="test")
     * @param MessageBusInterface $bus
     * @return Response
     */
    public function index(MessageBusInterface $bus): Response
    {
        $a =1;
        $b = 2;
        $c =$a + $b;

        $bus->dispatch(new MyTestMessage('TEST 123'));

        return $this->render('test/index.html.twig', [
            'controller_name' => 'TestController' . $c,
        ]);
    }
}

<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Training
 *
 * @ORM\Table(name="training", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_D5128A8FD5C9896F", columns={"journey_id"}), @ORM\UniqueConstraint(name="UNIQ_D5128A8F516A6958", columns={"email_invitation_id"}), @ORM\UniqueConstraint(name="UNIQ_D5128A8F77153098", columns={"code"})}, indexes={@ORM\Index(name="IDX_D5128A8F16FE72E1", columns={"updated_by"}), @ORM\Index(name="IDX_D5128A8F154FAF09", columns={"co_trainer_id"}), @ORM\Index(name="IDX_D5128A8FDE12AB56", columns={"created_by"}), @ORM\Index(name="IDX_D5128A8F979B1AD6", columns={"company_id"}), @ORM\Index(name="IDX_D5128A8FB213FA4", columns={"lang_id"}), @ORM\Index(name="IDX_D5128A8FFB08EDF6", columns={"trainer_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\TrainingRepository")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @Serializer\ExclusionPolicy("none")
 */
class Training
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Type("int")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="trainer_id", type="integer", nullable=true, options={"default"="NULL"})
     * @Type("int")
     */
    private $trainerId = 'NULL';

    /**
     * @var int|null
     *
     * @ORM\Column(name="co_trainer_id", type="integer", nullable=true, options={"default"="NULL"})
     * @Type("int")
     */
    private $coTrainerId = 'NULL';

    /**
     * @var int|null
     *
     * @ORM\Column(name="journey_id", type="integer", nullable=true, options={"default"="NULL"})
     * @Type("int")
     */
    private $journeyId = 'NULL';

    /**
     * @var int|null
     *
     * @ORM\Column(name="email_invitation_id", type="integer", nullable=true, options={"default"="NULL"})
     * @Type("int")
     */
    private $emailInvitationId = 'NULL';

    /**
     * @var int|null
     *
     * @ORM\Column(name="company_id", type="integer", nullable=true, options={"default"="NULL"})
     * @Type("int")
     */
    private $companyId = 'NULL';

    /**
     * @var int|null
     *
     * @ORM\Column(name="updated_by", type="integer", nullable=true, options={"default"="NULL"})
     * @Type("int")
     */
    private $updatedBy = 'NULL';

    /**
     * @var int|null
     *
     * @ORM\Column(name="created_by", type="integer", nullable=true, options={"default"="NULL"})
     * @Type("int")
     */
    private $createdBy = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", length=0, nullable=false)
     * @Type("string")
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="training_image", type="string", length=255, nullable=true, options={"default"="NULL"})
     * @Type("string")
     */
    private $trainingImage = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="short_description", type="text", length=0, nullable=true, options={"default"="NULL"})
     * @Type("string")
     */
    private $shortDescription = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="long_description", type="text", length=0, nullable=true, options={"default"="NULL"})
     * @Type("string")
     */
    private $longDescription = 'NULL';

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true, options={"default"="NULL"})
     * @Type("DateTime")
     */
    private $updatedAt = null;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     * @Type("bool")
     */
    private $isActive;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false, options={"default"="current_timestamp()"})
     * @Type("DateTime")
     */
    private $createdAt = 'current_timestamp()';

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="removed_at", type="datetime", nullable=true, options={"default"="NULL"})
     * @Type("DateTime")
     */
    private $removedAt = null;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="start_at", type="date", nullable=true, options={"default"="NULL"})
     * @Type("DateTime")
     */
    private $startAt = null;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="end_at", type="date", nullable=true, options={"default"="NULL"})
     * @Type("DateTime")
     */
    private $endAt = null;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_stopped", type="boolean", nullable=false)
     * @Type("bool")
     */
    private $isStopped;

    /**
     * @var string|null
     *
     * @ORM\Column(name="training_class", type="string", length=255, nullable=true, options={"default"="NULL"})
     * @Type("string")
     */
    private $trainingClass = 'NULL';

    /**
     * @var bool
     *
     * @ORM\Column(name="converted", type="boolean", nullable=false)
     * @Type("bool")
     */
    private $converted;

    /**
     * @var string|null
     *
     * @ORM\Column(name="extension", type="string", length=255, nullable=true, options={"default"="NULL"})
     * @Type("string")
     */
    private $extension = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=25, nullable=false)
     * @Type("string")
     */
    private $code;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="expired_code_at", type="date", nullable=true, options={"default"="NULL"})
     * @Type("DateTime")
     */
    private $expiredCodeAt = null;

    /**
     * @var bool
     *
     * @ORM\Column(name="invited_friend_status", type="boolean", nullable=false)
     * @Type("bool")
     */
    private $invitedFriendStatus;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="expired_invited_code_at", type="date", nullable=true, options={"default"="NULL"})
     * @Type("DateTime")
     */
    private $expiredInvitedCodeAt = null;

    /**
     * @var bool
     *
     * @ORM\Column(name="can_delete_answer", type="boolean", nullable=false)
     * @Type("bool")
     */
    private $canDeleteAnswer = '0';

    /**
     * @var bool
     *
     * @ORM\Column(name="is_shared", type="boolean", nullable=false, options={"default"="1"})
     * @Type("bool")
     */
    private $isShared = true;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="super_co_trainer", type="boolean", nullable=true)
     * @Type("bool")
     */
    private $superCoTrainer = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="smallint", nullable=false)
     * @Type("int")
     */
    private $type;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="scheduled_invitation", type="datetime", nullable=true, options={"default"="NULL"})
     * @Type("DateTime")
     */
    private $scheduledInvitation = null;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="finish_creating_at", type="datetime", nullable=true, options={"default"="NULL"})
     * @Type("DateTime")
     */
    private $finishCreatingAt = null;

    /**
     * @var int
     *
     * @ORM\Column(name="trainer_title", type="integer", nullable=false)
     * @Type("int")
     */
    private $trainerTitle;

    /**
     * @var int|null
     *
     * @ORM\Column(name="lang_id", type="integer", nullable=true, options={"default"="NULL"})
     * @Type("int")
     */
    private $langId = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="image_original_name", type="string", length=255, nullable=true, options={"default"="NULL"})
     * @Type("string")
     */
    private $imageOriginalName = 'NULL';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="notify_co_trainer", type="boolean", nullable=true)
     * @Type("bool")
     */
    private $notifyCoTrainer = '0';

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Training
     */
    public function setId(int $id): Training
    {
        $this->id = $id;
        return $this;
    }

    public function getTrainerId(): ?int
    {
        return $this->trainerId;
    }

    public function setTrainerId(?int $trainerId): self
    {
        $this->trainerId = $trainerId;

        return $this;
    }

    public function getCoTrainerId(): ?int
    {
        return $this->coTrainerId;
    }

    public function setCoTrainerId(?int $coTrainerId): self
    {
        $this->coTrainerId = $coTrainerId;

        return $this;
    }

    public function getJourneyId(): ?int
    {
        return $this->journeyId;
    }

    public function setJourneyId(?int $journeyId): self
    {
        $this->journeyId = $journeyId;

        return $this;
    }

    public function getEmailInvitationId(): ?int
    {
        return $this->emailInvitationId;
    }

    public function setEmailInvitationId(?int $emailInvitationId): self
    {
        $this->emailInvitationId = $emailInvitationId;

        return $this;
    }

    public function getCompanyId(): ?int
    {
        return $this->companyId;
    }

    public function setCompanyId(?int $companyId): self
    {
        $this->companyId = $companyId;

        return $this;
    }

    public function getUpdatedBy(): ?int
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?int $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTrainingImage(): ?string
    {
        return $this->trainingImage;
    }

    public function setTrainingImage(?string $trainingImage): self
    {
        $this->trainingImage = $trainingImage;

        return $this;
    }

    public function getShortDescription(): ?string
    {
        return $this->shortDescription;
    }

    public function setShortDescription(?string $shortDescription): self
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    public function getLongDescription(): ?string
    {
        return $this->longDescription;
    }

    public function setLongDescription(?string $longDescription): self
    {
        $this->longDescription = $longDescription;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getRemovedAt(): ?DateTimeInterface
    {
        return $this->removedAt;
    }

    public function setRemovedAt(?DateTimeInterface $removedAt): self
    {
        $this->removedAt = $removedAt;

        return $this;
    }

    public function getStartAt(): ?DateTimeInterface
    {
        return $this->startAt;
    }

    public function setStartAt(?DateTimeInterface $startAt): self
    {
        $this->startAt = $startAt;

        return $this;
    }

    public function getEndAt(): ?DateTimeInterface
    {
        return $this->endAt;
    }

    public function setEndAt(?DateTimeInterface $endAt): self
    {
        $this->endAt = $endAt;

        return $this;
    }

    public function getIsStopped(): ?bool
    {
        return $this->isStopped;
    }

    public function setIsStopped(bool $isStopped): self
    {
        $this->isStopped = $isStopped;

        return $this;
    }

    public function getTrainingClass(): ?string
    {
        return $this->trainingClass;
    }

    public function setTrainingClass(?string $trainingClass): self
    {
        $this->trainingClass = $trainingClass;

        return $this;
    }

    public function getConverted(): ?bool
    {
        return $this->converted;
    }

    public function setConverted(bool $converted): self
    {
        $this->converted = $converted;

        return $this;
    }

    public function getExtension(): ?string
    {
        return $this->extension;
    }

    public function setExtension(?string $extension): self
    {
        $this->extension = $extension;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getExpiredCodeAt(): ?DateTimeInterface
    {
        return $this->expiredCodeAt;
    }

    public function setExpiredCodeAt(?DateTimeInterface $expiredCodeAt): self
    {
        $this->expiredCodeAt = $expiredCodeAt;

        return $this;
    }

    public function getInvitedFriendStatus(): ?bool
    {
        return $this->invitedFriendStatus;
    }

    public function setInvitedFriendStatus(bool $invitedFriendStatus): self
    {
        $this->invitedFriendStatus = $invitedFriendStatus;

        return $this;
    }

    public function getExpiredInvitedCodeAt(): ?DateTimeInterface
    {
        return $this->expiredInvitedCodeAt;
    }

    public function setExpiredInvitedCodeAt(?DateTimeInterface $expiredInvitedCodeAt): self
    {
        $this->expiredInvitedCodeAt = $expiredInvitedCodeAt;

        return $this;
    }

    public function getCanDeleteAnswer(): ?bool
    {
        return $this->canDeleteAnswer;
    }

    public function setCanDeleteAnswer(bool $canDeleteAnswer): self
    {
        $this->canDeleteAnswer = $canDeleteAnswer;

        return $this;
    }

    public function getIsShared(): ?bool
    {
        return $this->isShared;
    }

    public function setIsShared(bool $isShared): self
    {
        $this->isShared = $isShared;

        return $this;
    }

    public function getSuperCoTrainer(): ?bool
    {
        return $this->superCoTrainer;
    }

    public function setSuperCoTrainer(?bool $superCoTrainer): self
    {
        $this->superCoTrainer = $superCoTrainer;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getScheduledInvitation(): ?DateTimeInterface
    {
        return $this->scheduledInvitation;
    }

    public function setScheduledInvitation(?DateTimeInterface $scheduledInvitation): self
    {
        $this->scheduledInvitation = $scheduledInvitation;

        return $this;
    }

    public function getFinishCreatingAt(): ?DateTimeInterface
    {
        return $this->finishCreatingAt;
    }

    public function setFinishCreatingAt(?DateTimeInterface $finishCreatingAt): self
    {
        $this->finishCreatingAt = $finishCreatingAt;

        return $this;
    }

    public function getTrainerTitle(): ?int
    {
        return $this->trainerTitle;
    }

    public function setTrainerTitle(int $trainerTitle): self
    {
        $this->trainerTitle = $trainerTitle;

        return $this;
    }

    public function getLangId(): ?int
    {
        return $this->langId;
    }

    public function setLangId(?int $langId): self
    {
        $this->langId = $langId;

        return $this;
    }

    public function getImageOriginalName(): ?string
    {
        return $this->imageOriginalName;
    }

    public function setImageOriginalName(?string $imageOriginalName): self
    {
        $this->imageOriginalName = $imageOriginalName;

        return $this;
    }

    public function getNotifyCoTrainer(): ?bool
    {
        return $this->notifyCoTrainer;
    }

    public function setNotifyCoTrainer(?bool $notifyCoTrainer): self
    {
        $this->notifyCoTrainer = $notifyCoTrainer;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue(): void
    {
        $this->createdAt = new DateTime('now');
    }
}

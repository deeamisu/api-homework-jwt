<?php

namespace App\MessageHandler;

use App\Message\MyTestMessage;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;


final class MyTestMessageHandler implements MessageHandlerInterface
{
    /** @var string $projectDir */
    private $projectDir;

    /**
     * MyTestMessageHandler constructor.
     * @param string $projectDir
     */
    public function __construct(string $projectDir)
    {
        $this->projectDir = $projectDir;
    }


    public function __invoke(MyTestMessage $message)
    {
        $filesystem = new Filesystem();
        $filesystem->touch($this->projectDir . '/public/myTest');
        $filesystem->appendToFile( $this->projectDir . '/public/myTest', $message->getContent());
    }
}

<?php


namespace App\Tests\Behat;

use Behat\Behat\Context\Context;
use Behat\Mink\Session;
use Symfony\Component\Routing\RouterInterface;

final class DemoMinkContext implements Context
{
    /** @var Session */
    private $session;

    /** @var RouterInterface */
    private $router;

    public function __construct(Session $session, RouterInterface $router)
    {
        $this->session = $session;
        $this->router = $router;
    }

    /**
     * @Then /^I visit "([^"]*)" page$/
     */
    public function iVisitPage()
    {
        $this->session->visit($this->router->generate('test'));
    }
}
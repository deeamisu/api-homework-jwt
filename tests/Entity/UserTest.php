<?php

namespace App\Tests\Entity;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testUserAtributes(): void
    {
        $user = new User();
        self::assertIsArray($user->getRoles());
        self::assertObjectHasAttribute('username', $user);
        self::assertObjectHasAttribute('password', $user);
    }
}
